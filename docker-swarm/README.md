# IRATA swarm deployment
This folder contains two docker-compose files for creating two stacks in a docker swarm.

One swarm for the acution-service itself and one for the management components.

The management components contain the ELK stack

## Starting the stacks
First the network called `management` which connects the two stacks needs to be created:

```
docker network create --driver=overlay --attachable management
```

The first stack to start is the ELK stack. Which can be done like this:

```
docker stack deploy --compose-file docker-compose-elk.yml elk
```

The second stack is the irata stack:

```
docker stack deploy --compose-file docker-compose-irata.yml irata
```

## Testing the stack
