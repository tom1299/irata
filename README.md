[![build status](https://gitlab.com/tom1299/irata/badges/master/build.svg)](https://gitlab.com/tom1299/irata/badges/master) [![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](http://tom1299.gitlab.io/irata)

# IRATA
If you read `IRATA` backwards its `ATARI`. The name of a well know software
company. And this little word trick is a reminiscence to the famous [C64](https://en.wikipedia.org/wiki/Commodore_64) game [M.U.L.E.](https://en.wikipedia.org/wiki/M.U.L.E.).

This game inspired me to create this project with the aim to build a game based on the ideas of this old `8-bit` game which is liked so much.

In addition this and the other projects (to follow) serve as the basis for me applying [Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/), [Docker swarm](https://docs.docker.com/engine/swarm/) and (if everything works out and I have enought time to do so) [Kubernetes](https://kubernetes.io/).
