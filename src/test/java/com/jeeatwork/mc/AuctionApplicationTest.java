package com.jeeatwork.mc;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jeeatwork.mc.model.Auction;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("memory")
public class AuctionApplicationTest {
	
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

	@Test
	public void test() throws Exception {
		HttpEntity<Void> request = new HttpEntity<Void>((Void)null);
		ResponseEntity<Auction> response = this.restTemplate
				  .exchange("http://localhost:" + port + "/auction/create/food", HttpMethod.POST, request, Auction.class);
		Auction auction = response.getBody();
		assertThat(auction, notNullValue());
	}
}
