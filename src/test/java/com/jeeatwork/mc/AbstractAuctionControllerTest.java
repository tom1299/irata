package com.jeeatwork.mc;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeeatwork.mc.model.Auction;
import com.jeeatwork.mc.model.Participant;
import com.jeeatwork.mc.model.Role;

public abstract class AbstractAuctionControllerTest {
	
    @Autowired
    private MockMvc mockMvc;

    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();
    
    @Test
    public void testCreateAndDelete() throws Exception {
        Auction auction = createAuction();
        assertThat(auction, is(notNullValue()));
        assertThat(auction.getId(), is(notNullValue()));
        assertThat(auction.getGood(), is(equalTo("food")));
        this.deleteAuction(auction);
    }
    
    @Test
    public void testCreateRegisterAndDelete() throws Exception {
        Auction auction = createAuction();
        Participant participant = this.registerParticipant(auction, Role.Buyer);
        assertParticipant(participant, Role.Buyer);
        this.deleteAuction(auction);
    }
    
    @Test
    public void testCreateRegisterAndBid() throws Exception {
        Auction auction = createAuction();
        Participant participant = this.registerParticipant(auction, Role.Buyer);
        assertParticipant(participant, Role.Buyer);
        assertThat(this.placeBid(auction, participant, 100), is(true));
        this.deleteAuction(auction);
    }
    
    @Test
    public void testCreateRegisterAndAsk() throws Exception {
        Auction auction = createAuction();
        Participant participant = this.registerParticipant(auction, Role.Seller);
        assertParticipant(participant, Role.Seller);
        assertThat(this.placeAsk(auction, participant, 100), is(true));
        this.deleteAuction(auction);
    }
    
    @Test
    public void testBidWithInvalidAuctionId() throws Exception {
    	Auction auction = new Auction();
    	auction.setId("1234");
    	Participant participant = new Participant();
    	participant.setId("5678");
        assertThat(this.placeBid(auction, participant, 100), is(false));
    }
    
    @Test
    public void testStatus() throws Exception {
    	Auction auction = createAuction();
    	Participant participant = this.registerParticipant(auction, Role.Seller);
    	assertThat(this.placeAsk(auction, participant, 100), is(true));
    	auction = this.getStatus(auction);
    	assertThat(participant.getId(), equalTo(auction.getParticipants().get(participant.getId()).getId()));
    }
    
	private void assertParticipant(Participant participant, Role role) {
		assertThat(participant, is(notNullValue()));
        assertThat(participant.getId(), is(notNullValue()));
        assertThat(participant.getRole(), is(equalTo(role)));
	}

	private Auction createAuction() throws Exception {
		byte[] content = this.mockMvc.perform(post("/auction/create/food")).andExpect(status().isCreated()).andReturn().getResponse().getContentAsByteArray();
        Auction auction = OBJECTMAPPER.readValue(content, Auction.class);
		return auction;
	}

	private void deleteAuction(Auction auction) throws Exception {
		this.mockMvc.perform(post("/auction/delete/" + auction.getId())).andExpect(status().isOk());
	}
	
	private Participant registerParticipant(Auction auction, Role role) throws Exception {
		byte[] content = this.mockMvc.perform(post("/auction/register/" + auction.getId() + "/" + role)).andExpect(status().isOk()).andReturn().getResponse().getContentAsByteArray();
		Participant participant = OBJECTMAPPER.readValue(content, Participant.class);
		return participant;
	}
	
	private boolean placeBid(Auction auction, Participant participant, int amount) throws Exception {
		int status = this.mockMvc.perform(post("/auction/bid/" + auction.getId() + "/" + participant.getId() + "/" + amount)).andReturn().getResponse().getStatus();
		return status == HttpServletResponse.SC_OK;
	}
	
	private boolean placeAsk(Auction auction, Participant participant, int amount) throws Exception {
		int status = this.mockMvc.perform(post("/auction/ask/" + auction.getId() + "/" + participant.getId() + "/" + amount)).andReturn().getResponse().getStatus();
		return status == HttpServletResponse.SC_OK;
	}
	
	private Auction getStatus(Auction auction) throws Exception {
		byte[] content = this.mockMvc.perform(get("/auction/status/" + auction.getId())).andExpect(status().isOk()).andReturn().getResponse().getContentAsByteArray();
		Auction retValue = OBJECTMAPPER.readValue(content, Auction.class);
		return retValue;
	}
}

