package com.jeeatwork.mc;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.stream.IntStream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.jeeatwork.mc.model.Auction;
import com.jeeatwork.mc.model.Participant;
import com.jeeatwork.mc.model.Role;

public class BiddingTestScenario {
	
	private String acutionServiceUrl = "http://localhost:8080/auction";
	
    @Rule
    public ErrorCollector collector= new ErrorCollector();
	
	private RestTemplate restTemplate;

	public BiddingTestScenario() {
		super();
	}
	
	@Test
	public void test() throws Exception {
		this.restTemplate = new RestTemplate();
		
		Auction auction = this.createAuction();
		
		assertThat(auction, notNullValue());
		
		Participant participant = this.registerParticipant(auction, Role.Seller);
		
		assertThat(participant, notNullValue());
		
		IntStream.rangeClosed(1, 100).forEach(i -> this.placeBid(auction, participant, i));
		
		System.out.println("Bid test scenario finished");
	}
	
	Auction createAuction() {
		try {
			HttpEntity<Void> request = new HttpEntity<Void>((Void)null);
			ResponseEntity<Auction> response = this.restTemplate
					  .exchange(this.acutionServiceUrl + "/create/food", HttpMethod.POST, request, Auction.class);
			if (!response.getStatusCode().equals(HttpStatus.CREATED)) {
				this.collector.addError(new Exception("/create/food returned " + response.getStatusCode() + " instead of " + HttpStatus.CREATED));
			}
			return response.getBody();
		}
		catch (Exception e) {
			this.collector.addError(e);
		}
		
		return null;
	}
	
	private Participant registerParticipant(Auction auction, Role role) {
		try {
			HttpEntity<Void> request = new HttpEntity<Void>((Void) null);
			ResponseEntity<Participant> response = this.restTemplate.exchange(
					this.acutionServiceUrl + "/register/" + auction.getId() + "/" + role, HttpMethod.POST, request,
					Participant.class);
			if (!response.getStatusCode().equals(HttpStatus.OK)) {
				this.collector.addError(new Exception(
						"/register returned " + response.getStatusCode() + " instead of " + HttpStatus.OK));
			}
			return response.getBody();
		}
		catch (Exception e) {
			this.collector.addError(e);
		}
		return null;
	}
	
	void placeBid(Auction auction, Participant participant, int amount) {
		try {
			HttpEntity<Void> request = new HttpEntity<Void>((Void) null);
			ResponseEntity<Participant> response = this.restTemplate.exchange(
					this.acutionServiceUrl + "/bid/" + auction.getId() + "/" + participant.getId() + "/" + amount,
					HttpMethod.POST, request, Participant.class);
			if (!response.getStatusCode().equals(HttpStatus.OK)) {
				this.collector.addError(
						new Exception("/bid returned " + response.getStatusCode() + " instead of " + HttpStatus.OK));
			}
		}
		catch (Exception e) {
			this.collector.addError(e);
		}
	}
}
