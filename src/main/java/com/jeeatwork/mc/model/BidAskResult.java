package com.jeeatwork.mc.model;

public enum BidAskResult {
	PLACED,
	AUCTION_NOT_FOUND,
	NOT_REGISTERED;
}
