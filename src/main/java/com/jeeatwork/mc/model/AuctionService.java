package com.jeeatwork.mc.model;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jeeatwork.mc.persistence.AuctionRepository;

@Component("auctionService")
public class AuctionService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AuctionService.class);
	
	@Autowired
	private AuctionRepository auctionRepository;

	public Auction create(String good) {
		Auction auction = new Auction();
		String id = createId();
		auction.setId(id);
		auction.setGood(good);
		auction.setStatus(Status.READY);
		this.auctionRepository.update(auction);
		LOG.info("Created auction for good \"" + good + "\"");
		return auction;
	}
	
	public Auction get(String auctionId) {
		return this.auctionRepository.read(auctionId);
	}
	
	public boolean delete(String auctionId) {
		LOG.info("Deleting auction with id \"" + auctionId + "\"");
		return this.auctionRepository.delete(auctionId);
	}
	
	public boolean start(String auctionId) {
		return setStatus(auctionId, Status.ONGOING);
	}

	public boolean stop(String auctionId) {
		return setStatus(auctionId, Status.STOPPED);
	}

	private boolean setStatus(String auctionId, Status status) {
		Auction auction = this.auctionRepository.read(auctionId);
		boolean stopped = false;
		if (auction != null) {
			auction.setStatus(status);
			stopped = true;
		}
		return stopped;
	}
	
	public Participant register(String auctionId, Participant participant) {
		Auction auction = this.auctionRepository.read(auctionId);
		participant.setId(this.createId());
		auction.getParticipants().put(participant.getId(), participant);
		this.auctionRepository.update(auction);
		LOG.info("Registered participant \"" + participant.getId() + "\" for auction \"" + auctionId + "\"");
		return participant;
	}
	
	public BidAskResult bidOrAsk(String auctionId, String participantId, int price) {
		BidAskResult bidResult = BidAskResult.PLACED;
		Auction auction = this.auctionRepository.read(auctionId);
		if (auction == null) {
			return BidAskResult.AUCTION_NOT_FOUND;
		}
		
		Participant participant = auction.getParticipants().get(participantId);
		
		if (participant == null) {
			return BidAskResult.NOT_REGISTERED;
		}

		participant.setPrice(price);
		
		LOG.info("Placed bid of " + price + " for auction " + auctionId + " for participant " + participantId);
		return bidResult;
	}
	
	private String createId() {
		return UUID.randomUUID().toString();
	}
}
