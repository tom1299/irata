package com.jeeatwork.mc.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Auction implements Serializable {
	

	private static final long serialVersionUID = 1637235689358018116L;
	
	private String id;

	private Status status;
	
	private String good;

	private Map<String, Participant> participants = new HashMap<>();

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getGood() {
		return good;
	}

	public void setGood(String good) {
		this.good = good;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Map<String, Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(Map<String, Participant> participants) {
		this.participants = participants;
	}
}
