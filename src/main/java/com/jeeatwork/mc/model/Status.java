package com.jeeatwork.mc.model;

public enum Status {
	READY,
	ONGOING,
	STOPPED;
}
