package com.jeeatwork.mc.model;

import java.io.Serializable;

public class Participant implements Serializable {
	
	private static final long serialVersionUID = -1207845996828991034L;

	private String id;
	
	private Role role;

	private int price;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
