package com.jeeatwork.mc;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jeeatwork.mc.model.Auction;
import com.jeeatwork.mc.model.AuctionService;
import com.jeeatwork.mc.model.BidAskResult;
import com.jeeatwork.mc.model.Participant;
import com.jeeatwork.mc.model.Role;

@RestController
@RequestMapping(value = "/auction", produces = "application/json")
public class AuctionController {
	
	private static final Map<BidAskResult, HttpStatus> BIDASK_MAPPING = new HashMap<>();
	
	@Autowired
	private AuctionService auctionService;

	@RequestMapping(value = "/create/{good}", method = RequestMethod.POST)
	public ResponseEntity<Auction> create(@PathVariable String good) {
		return new ResponseEntity<>(this.auctionService.create(good), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/delete/{auctionId}", method = RequestMethod.POST)
	public ResponseEntity<Void> delete(@PathVariable String auctionId) {
		boolean removed = this.auctionService.delete(auctionId);
		HttpStatus removeStatus = removed ? HttpStatus.OK : HttpStatus.NOT_FOUND; 
		return new ResponseEntity<Void>(removeStatus);
	}
	
	@RequestMapping(value = "/status/{auctionId}", method = RequestMethod.GET)
	public ResponseEntity<Auction> status(@PathVariable String auctionId) {
		Auction auction = this.auctionService.get(auctionId);
		HttpStatus responseStatus = auction != null ? HttpStatus.OK : HttpStatus.NOT_FOUND; 
		return new ResponseEntity<>(auction, responseStatus);
	}

	@RequestMapping(value = "/register/{auctionId}/{role}", method = RequestMethod.POST)
	public ResponseEntity<Participant> register(@PathVariable String auctionId, @PathVariable String role) {
		Participant participant = new Participant();
		participant.setRole(Role.valueOf(role));
		return new ResponseEntity<>(this.auctionService.register(auctionId, participant), HttpStatus.OK);
	}
	
	@RequestMapping(value = {"/bid/{auctionId}/{participantId}/{price}",
			"/ask/{auctionId}/{participantId}/{price}"},
			method = RequestMethod.POST)
	public ResponseEntity<Void> bid(@PathVariable String auctionId, @PathVariable String participantId, @PathVariable int price) {
		BidAskResult result = this.auctionService.bidOrAsk(auctionId, participantId, price);
		return new ResponseEntity<>(BIDASK_MAPPING.get(result));
	}
	
	@PostConstruct
	public void initBidAskMapping() {
		BIDASK_MAPPING.put(BidAskResult.AUCTION_NOT_FOUND, HttpStatus.NOT_FOUND);
		BIDASK_MAPPING.put(BidAskResult.NOT_REGISTERED, HttpStatus.NOT_FOUND);
		BIDASK_MAPPING.put(BidAskResult.PLACED, HttpStatus.OK);
	}
}
