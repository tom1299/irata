package com.jeeatwork.mc.persistence.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@Profile("redis")
public class RedisConfiguration {
	
    @Value("${auction-service.redis.port}")
    private int redisPort;
    
    @Value("${auction-service.redis.host}")
    private String redisHost;

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory cf = new JedisConnectionFactory(new RedisStandaloneConfiguration(this.redisHost, this.redisPort));
		return cf;
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}
}
