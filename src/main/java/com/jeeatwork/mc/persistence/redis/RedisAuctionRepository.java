/**
 * 
 */
package com.jeeatwork.mc.persistence.redis;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.jeeatwork.mc.model.Auction;
import com.jeeatwork.mc.persistence.AuctionRepository;

@Component
@SuppressWarnings("rawtypes")
@Profile("redis")
public class RedisAuctionRepository implements AuctionRepository {
	
	private static final String KEY = "Auction";
	
	@Autowired
	private RedisTemplate<String, Object> redis;
	
	private HashOperations hashOperations;

	@SuppressWarnings("unchecked")
	@Override
	public Auction read(String id) {
		return (Auction) hashOperations.get(KEY, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update(Auction auction) {
		this.hashOperations.put(KEY, auction.getId(), auction);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean delete(String id) {
		return hashOperations.delete(KEY, id) != null;
	}
	
    @PostConstruct
    private void init(){
        hashOperations = redis.opsForHash();
    }
}
