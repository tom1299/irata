/**
 * 
 */
package com.jeeatwork.mc.persistence.memory;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.jeeatwork.mc.model.Auction;
import com.jeeatwork.mc.persistence.AuctionRepository;

@Component
@Profile("memory")
public class MemoryAuctionRepository implements AuctionRepository {
	
	private Map<String, Auction> auctions = new HashMap<>();

	public MemoryAuctionRepository() {
		super();
	}

	@Override
	public Auction read(String id) {
		return this.auctions.get(id);
	}

	@Override
	public void update(Auction auction) {
		this.auctions.put(auction.getId(), auction);
	}

	@Override
	public boolean delete(String id) {
		return this.auctions.remove(id) != null;
	}

}
