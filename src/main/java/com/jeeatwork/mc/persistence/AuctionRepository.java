package com.jeeatwork.mc.persistence;

import com.jeeatwork.mc.model.Auction;

public interface AuctionRepository {

	Auction read(String id);
	
	void update(Auction auction);
	
	boolean delete(String id);
}
