package com.jeeatwork.mc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.jeeatwork.mc.persistence.redis.RedisConfiguration;

@SpringBootApplication
@Import(RedisConfiguration.class)
public class AuctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuctionApplication.class, args);
	}
}
