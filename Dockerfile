FROM registry.gitlab.com/tom1299/irata-docker/irata-base
VOLUME /tmp
COPY ./build/libs/auction-service.jar /app.jar
COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 8080:8080
