#!/bin/sh
set -e

cd /

# Start filebeat
filebeat -c /filebeat.yml -d "publish" &

# Start the service
java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
